package hsoft.model;

import java.util.List;

public class String {
    private java.lang.String productId;
    private java.lang.String nameProduct;
    private Double price;
    private List<Double> faiValues;
    public String() {
    }

    public String(java.lang.String productId, java.lang.String nameProduct, Double price, List<Double> faiValues) {
        this.productId = productId;
        this.nameProduct = nameProduct;
        this.price = price;
        this.faiValues = faiValues;
    }

    public java.lang.String getProductId() {
        return productId;
    }

    public void setProductId(java.lang.String productId) {
        this.productId = productId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }


    public java.lang.String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(java.lang.String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public List<Double> getFaiValues() {
        return faiValues;
    }

    public void setFaiValues(List<Double> faiValues) {
        this.faiValues = faiValues;
    }
}
