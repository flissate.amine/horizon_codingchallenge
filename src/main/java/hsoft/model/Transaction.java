package hsoft.model;

public class Transaction {
    private String idProduct;
    private Double price;
    private Long quantity;

    public Transaction() {
    }

    public Transaction(String idProduct, Double price, Long quantity) {
        this.idProduct = idProduct;
        this.price = price;
        this.quantity = quantity;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    @Override
    public java.lang.String toString() {
        return "Transaction{" +
                "idProduct=" + idProduct +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
