package hsoft;

import com.hsoft.codingtest.DataProvider;
import com.hsoft.codingtest.DataProviderFactory;
import com.hsoft.codingtest.MarketDataListener;
import com.hsoft.codingtest.PricingDataListener;
import hsoft.model.String;
import org.apache.log4j.FileAppender;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class VwapTrigger {

    static final Logger logger = Logger.getLogger(VwapTrigger.class);

    public static void main(String[] args) {


        DataProvider provider = DataProviderFactory.getDataProvider();
        Map<String, List<Double>> Enregistrements = new HashMap<>();
        List<Double> fairValues = new ArrayList<>();
        HashSet<Double> listVWAP = new HashSet<>();
        Map<String, List<Double>> transactions = new HashMap<>();
        AtomicReference<Double> vwap = new AtomicReference<>();
        AtomicReference<Double> lastFairValue = new AtomicReference<>();

        provider.addMarketDataListener(new MarketDataListener() {
            public void transactionOccured(String productId, long quantity, double price) {
                double currentVWAP = quantity * price / quantity;
                //impl a map of product id and i'ts VWM
                transactions.put(productId, listVWAP.stream()
                        .limit(5)
                        .map(VWAP -> currentVWAP).collect(Collectors.toList()));
                //get average of all values for each product id
                Map<String, Double> average = transactions.entrySet()
                        .stream()
                        .collect(Collectors.toMap(Map.Entry::getKey,
                                e -> e.getValue()
                                        .stream()
                                        .mapToDouble(Double::doubleValue)
                                        .average()
                                        .getAsDouble()));
                //stock the average value for each productID into an atomic variable
                average.forEach(
                        (string, aDouble) -> vwap.set(aDouble)
                );
                // Do the comparaison with last Value
                if (vwap.get() > lastFairValue.get()) {
                    logger.info("VMWR" + vwap.get() + ">" + lastFairValue.get());
                }
                for (Map.Entry<String, List<Double>> enr : Enregistrements.entrySet()) {
                    if (enr.getKey().equals(productId)) {
                        if (vwap.get() > lastFairValue.get()) {
                            logger.info("VMWR for " + enr.getKey() + "" + vwap.get() + " > last fair value : " + lastFairValue.get());
                            logger.addAppender(new FileAppender());
                        }
                    }
                }
            }
        });
        provider.addPricingDataListener(new PricingDataListener() {
            public void fairValueChanged(String productId, double fairValue) {
                if (!(productId == null)) {
                    try {
                        Enregistrements.put(productId, fairValues.stream().map(value -> fairValue).collect(Collectors.toList()));
                        long count = Enregistrements.values().size();
                        for (Map.Entry<String, List<Double>> row : Enregistrements.entrySet()) {
                            Double currenFaiValue = row.getValue().stream().skip(count - 1).findFirst().get();
                            lastFairValue.set(currenFaiValue);
                            if (currenFaiValue == fairValue) {
                                logger.info("value unchanged for :" + productId);
                            }
                            logger.info("new value for " + productId + ":" + currenFaiValue);
                        }
                    } catch (Exception e) {
                        logger.error("Something goes wrong" + e);
                    }
                    if (vwap.get() > lastFairValue.get()) {
                        logger.info("VMWR" + vwap.get() + ">" + lastFairValue.get());
                    }
                }
                logger.error("product ID cannont be null");
            }
        });

        provider.listen();
        // When this method returns, the test is finished and you can check your results in the console
        LogManager.shutdown();
    }
}
